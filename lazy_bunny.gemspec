# -*- encoding: utf-8 -*-
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'lazy_bunny/version'

Gem::Specification.new do |gem|
  gem.name          = "lazy_bunny"
  gem.version       = LazyBunny::VERSION
  gem.authors       = ["Luciano Di Lucrezia"]
  gem.email         = ["luciano.dilucrezia@gmail.com"]
  gem.description   = %q{An overly simplistic wrapper around Bunny, with sane defaults for the lazy.}
  gem.summary       = %q{Simple wrapper around Bunny.}
  gem.homepage      = ""

  gem.files         = `git ls-files`.split($/)
  gem.executables   = gem.files.grep(%r{^bin/}).map{ |f| File.basename(f) }
  gem.test_files    = gem.files.grep(%r{^(test|spec|features)/})
  gem.require_paths = ["lib"]

  gem.add_runtime_dependency 'bunny', '~> 0.8'
end
