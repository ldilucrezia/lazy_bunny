require 'bunny'

module LazyBunny

  class Queue

    def initialize(params)
      @host          = params[:host] or raise "Parameter :host is required."
      @username      = params[:username]   || 'guest'
      @password      = params[:password]   || 'guest'
      @queue_name    = params[:queue]      || 'test'
      @exchange_name = params[:exchange]   || 'test-exchange'
      @durable       = params[:durable]    || false
      @mandatory     = params[:mandatory]  || false
      @persistent    = params[:persistent] || false
      @bunny         = Bunny.new(:host => @host, :user => @username, :pass => @password).tap { |b| b.start }
      @queue         = @bunny.queue(@queue_name, :durable => @durable)
      @exchange      = @bunny.exchange(@exchange_name, :durable => @durable)
      @queue.bind(@exchange_name)
    end

    def logger
      @logger || Logger.new('/dev/null')
    end

    def logger=(logger)
      @logger = logger
    end

    def read
      @queue.pop[:payload]
    end

    def write(str)
      @exchange.publish(str, :mandatory => @mandatory, :persistent => @persistent)
    end

    def length
      @queue.status[:message_count]
    end

    def to_a
      a = []
      length.times { a << read }
      a
    end

    def delete!
      @queue.delete
    end

  end

end
